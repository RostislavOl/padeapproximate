/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package constants;

public class ListsConstants {

    public final String RAVNOM_RASP = "Равномерное распределение";
    public final String EXP_RASP = "Экспоненциальное распределение";
    public final String COCHI_RASP = "Распределение Коши";
    public final String LEVI_RASP = "Распределение Леви";
    public final String LOG_RASP = "Логистическое распределение";
    public final String LAPLAS_RASP = "Распределение Лапласа";

    public final String GAUSS_KERNEL = "Ядро Гаусса";
    public final String EXP_KERNEL = "Экспоненциальное Ядро";
    public final String EPANECHNIKOV_KERNEL = "Ядро Епанечникова";
    public final String COSINUS_KERNEL = "Косинусное Ядро";
    public final String TRIANGLE_KERNEL = "Треугольное Ядро";
    public final String THIRD_CUBE_KERNEL = "Трикубическое Ядро";
    public final String SILVERMAN_KERNEL = "Ядро Сильвермана";
    public final String SIGMOIDAL_KERNEL = "Сигмоидальное Ядро";
    public final String THIRD_QUAD_KERNEL = "Триквадратное Ядро";
    public final String FISHER_KERNEL = "Ядро Фишера";

}

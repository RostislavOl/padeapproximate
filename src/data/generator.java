/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package data;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.Scanner;

/**
 *
 * @author rool0816
 */
public class generator {

    private List<Double> generatedMas = new ArrayList<Double>();
    private int num = 10000;

    public int getNum() {
        return num;
    }

    public void setNum(int num) {
        this.num = num;
    }
    
    public List<Double> getFloatFromFile(File file) throws FileNotFoundException {
        try {
            Scanner sc = new Scanner(file);
            while (sc.hasNext()) {
               generatedMas.add(sc.nextDouble());
            }
        }
    catch(FileNotFoundException e){
            e.printStackTrace();
    }
    return generatedMas;
}
    
    public List<Double> getGeneratedMas() {
        return generatedMas;
    }
    
    
    public List<Double> getRavnomernRand(double a, double b) {
        if (generatedMas.isEmpty() == false) {
            generatedMas.clear();
        }
        Random r = new Random();
        for (int i = 0; i < num; i++) {
            generatedMas.add(r.nextDouble() * (b - a) + a);
        }
        return generatedMas;
    }


    public List<Double> getExpRand(double a, double b, double lambda) {
        if (!generatedMas.isEmpty()) {
            generatedMas.clear();
        }
        Random r = new Random();
        for (int i = 0; i < num; i++) {
            generatedMas.add(((float) Math.log(1 - r.nextDouble()) / (-lambda)) * (b/a)+a);
        }
        return generatedMas;
    }

    public List<Double> getCoshiRand(double a, double b) {
        if (!generatedMas.isEmpty()) {
            generatedMas.clear();
        }
        Random r = new Random();
        for (int i = 0; i < num; i++) {
            double x;
            double y;
            do{
                x = r.nextDouble() * (1 + 1) - 1;
                y = r.nextDouble() * (1 + 1) - 1;
            }
            while(x * x + y * y > 1.0 || y == 0.0);
            generatedMas.add(a + b * (x / y));
        }
        return generatedMas;
    }

    public List<Double> getLeviRand(double mu, double c) {
        if (!generatedMas.isEmpty()) {
            generatedMas.clear();
        }
        Random r = new Random();
        for (int i = 0; i < num; i++) {
            generatedMas.add((mu + c/(r.nextDouble())));
        }
        return generatedMas;
    }
    
    public List<Double> getLogisticRand(double mu, double a){
        if (!generatedMas.isEmpty()) {
            generatedMas.clear();
        }
        Random r = new Random();
        for (int i = 0; i < num; i++) {
            generatedMas.add((mu + a*Math.log(r.nextDouble())));
        }
        return generatedMas;
    }
    
    public List<Double> getLaplassianRand(double mu, double a, double b, double lambda){
        if (!generatedMas.isEmpty()) {
            generatedMas.clear();
        }
        Random r = new Random();
        for (int i = 0; i < num; i++) {
            generatedMas.add(mu + (r.nextDouble() > 0 ? (Math.log(1 - r.nextDouble()) / (-lambda)* (b/a)+a) : -(Math.log(1 - r.nextDouble()) / (-lambda)* (b/a)+a)));
        }
        return generatedMas;
    }
    
    public List<Double> getLogNormRand(double a, double b){
        if (!generatedMas.isEmpty()) {
            generatedMas.clear();
        }
        Random r = new Random();
        for (int i = 0; i < num; i++) {
            generatedMas.add(Math.exp(r.nextDouble() * (b - a) + a));
        }
        return generatedMas;
    }
    
    public List<Double> getNormRand(){
        if (!generatedMas.isEmpty()) {
            generatedMas.clear();
        }
        
        return generatedMas;
    }
    
    public List<Double> getPoissonRand(double a, double b, double lambda){
        if (!generatedMas.isEmpty()) {
            generatedMas.clear();
        }
        
        return generatedMas;
    }
    
    public List<Double> getStudentRand(double nu){
        if (!generatedMas.isEmpty()) {
            generatedMas.clear();
        }
        
        return generatedMas;
    }
    
}

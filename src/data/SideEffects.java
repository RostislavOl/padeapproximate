/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package data;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;
import javafx.stage.FileChooser;

/**
 *
 * @author Rost
 */
public class SideEffects {

    File selectedFile;

    public static final Path PATH = Paths.get("/home/rostislav/Documents/txt");

    public File getFile() {
        FileChooser fileChooser = new FileChooser();
        fileChooser.setTitle("Выберите файл");
        fileChooser.getExtensionFilters().addAll(
                new FileChooser.ExtensionFilter("XML Files", "*.xml"),
                new FileChooser.ExtensionFilter("All Files", "*.*")
        );

        selectedFile = fileChooser.showOpenDialog(null);

        if (selectedFile != null) {
        }
        //name.setText("");
        return selectedFile;
    }

    public void writeFile(String nameLaw, List<Double> d) throws IOException {
        ArrayList<Path> files = new ArrayList<>();
        files = (ArrayList<Path>) Files.list(PATH).collect(Collectors.toList());
        
        FileWriter filewriter = new FileWriter(new File("/home/rostislav/Documents/txt/"+nameLaw + files.size() + ".txt"));
        for(int a = 0; a < d.size(); a++){
            filewriter.write(d.get(a) + " ");
            filewriter.flush();
        }
    }

}

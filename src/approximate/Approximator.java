/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package approximate;

import java.util.ArrayList;
import java.util.List;

public class Approximator {

    MathOperations operations = new MathOperations();

    public List<Double> gaussianKernel(List<Double> x, double h) {
        if (h == 0) {
            h = operations.silvermanBandtwith(x);
        }
        List<Double> newx = new ArrayList<>();
        for (int j = 0; j < x.size(); j++) {
            double singlex = 0;
            for (int i = 0; i < x.size(); i++) {
                singlex += (0.3989422804) * Math.exp(-((Math.pow((x.get(i) - x.get(j) / h), 2)) / 2));
            }
            newx.add(singlex / (x.size() * h));
        }
        return newx;

    }

    public List<Double> exponentialKernel(List<Double> x, double h) {
        if (h == 0) {
            h = operations.silvermanBandtwith(x);
        }
        List<Double> newx = new ArrayList<>();
        for (int j = 0; j < x.size(); j++) {
            double singlex = 0;
            for (int i = 0; i < x.size(); i++) {
                singlex += Math.exp(-Math.abs((x.get(i) - x.get(j)) / h)) / 2;
            }
            newx.add((singlex / (x.size() * h)) * 100);
        }
        return newx;
    }

    public List<Double> epanechnikovKernel(List<Double> x, double h) {
        if (h == 0) {
            h = operations.silvermanBandtwith(x);
        }
        List<Double> newx = new ArrayList<>();
        for (int j = 0; j < x.size(); j++) {
            double singlex = 0;
            for (int i = 0; i < x.size(); i++) {
                if (Math.abs((x.get(j) - x.get(i)) / h) <= 1) {
                    singlex += (0.75) * (Math.max(1.0 - Math.pow(((x.get(i) - x.get(j)) / h), 2), 0));
                } else {
                    singlex += 0.0;
                }
            }
            newx.add(singlex / (x.size() * h));
        }
        return newx;
    }

    public List<Double> triangeleKernel(List<Double> x, double h) {
        if (h == 0) {
            h = operations.silvermanBandtwith(x);
        }
        List<Double> newx = new ArrayList<>();
        for (int j = 0; j < x.size(); j++) {
            double singlex = 0;
            for (int i = 0; i < x.size(); i++) {
                if (Math.abs((x.get(j) - x.get(i)) / h) <= 1) {
                    singlex += Math.max(1 - Math.abs(((x.get(i) - x.get(j)) / h)), 0);
                } else {
                    singlex += 0.0;
                }
            }
            newx.add(singlex / (x.size() * h));
        }
        return newx;
    }

    public List<Double> cosinusKernel(List<Double> x, double h) {
        if (h == 0) {
            h = operations.silvermanBandtwith(x);
        }
        List<Double> newx = new ArrayList<>();
        for (int j = 0; j < x.size(); j++) {
            double singlex = 0;
            for (int i = 0; i < x.size(); i++) {
                if (Math.abs((x.get(j) - x.get(i)) / h) <= 1) {
                    singlex += (0.78539816339) * Math.cos(1.57079632679 * ((x.get(j) - x.get(i)) / h));
                }
            }
            newx.add(singlex / (x.size() * h));
        }
        return newx;
    }

    public List<Double> threeCubeKernel(List<Double> x, double h) {
        if (h == 0) {
            h = operations.silvermanBandtwith(x);
        }
        List<Double> newx = new ArrayList<>();
        for (int j = 0; j < x.size(); j++) {
            double singlex = 0;
            for (int i = 0; i < x.size(); i++) {
                if (Math.abs((x.get(i) - x.get(j)) / h) < 1) {
                    singlex += (0.86419753086) * Math.pow((1 - Math.pow(Math.abs((x.get(j) - x.get(i)) / h), 3)), 3);
                }
            }
            newx.add(singlex / (x.size() * h));
        }
        return newx;
    }

    public List<Double> silvermanKernel(List<Double> x, double h) {
        if (h == 0) {
            h = operations.silvermanBandtwith(x);
        }
        List<Double> newx = new ArrayList<>();
        for (int j = 0; j < x.size(); j++) {
            double singlex = 0;
            for (int i = 0; i < x.size(); i++) {
                singlex += (Math.exp(-Math.abs((x.get(j) - x.get(i)) / h) / Math.sqrt(2)) / 2) * Math.sin((Math.abs((x.get(j) - x.get(i)) / h) / Math.sqrt(2)) + 0.78539816339);
            }
            newx.add(singlex / (x.size() * h));
        }
        return newx;
    }

    public List<Double> sigmoidalKernel(List<Double> x, double h) {
        if (h == 0) {
            h = operations.silvermanBandtwith(x);
        }
        List<Double> newx = new ArrayList<>();
        for (int j = 0; j < x.size(); j++) {
            double singlex = 0;
            for (int i = 0; i < x.size(); i++) {
                singlex += 0.63661977236 / (Math.exp((x.get(j) - x.get(i)) / h) + Math.exp(-(x.get(j) - x.get(i)) / h));
            }
            newx.add(singlex / (x.size() * h));
        }
        return newx;
    }

    public List<Double> threeQuadKernell(List<Double> x, double h) {
        if (h == 0) {
            h = operations.silvermanBandtwith(x);
        }
        List<Double> newx = new ArrayList<>();
        for (int j = 0; j < x.size(); j++) {
            double singlex = 0;
            for (int i = 0; i < x.size(); i++) {
                if (Math.abs((x.get(i) - x.get(j)) / h) < 1) {
                    singlex += (1.59090909091) * Math.pow((1 - Math.pow((x.get(j) - x.get(i)) / h, 2)), 3);
                }
            }
            newx.add(singlex / (x.size() * h));
        }
        return newx;
    }

    public List<Double> fisherKernell(List<Double> x, double h) {
        if (h == 0) {
            h = operations.silvermanBandtwith(x);
        }
        List<Double> newx = new ArrayList<>();
        for (int j = 0; j < x.size(); j++) {
            double singlex = 0;
            double s = 0;
            for (int i = 0; i < x.size(); i++) {
                if (((x.get(i) - x.get(j)) / h) / 2 < Math.PI) {
                    s = (0.15915494309) * (Math.sin(((x.get(i) - x.get(j)) / h) / 2) / (((x.get(i) - x.get(j)) / h) / 2));
                    singlex += (s == s ? s : 0);
                    s = 0;
                }
            }
            newx.add(singlex / (x.size() * h));
        }
        return newx;
    }

}

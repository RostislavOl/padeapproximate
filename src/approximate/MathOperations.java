/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package approximate;

import data.SideEffects;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;


public class MathOperations {
    
    private int N = 15;

    public int getN() {
        return N;
    }

    public void setN(int N) {
        this.N = N;
    }
    
    SideEffects effects = new SideEffects();

    public double xAverage(List<Double> mas){
        double xs = 0;
        for (int i = 0; i < mas.size(); i++) {
            xs += mas.get(i);
        }
        return  xs / mas.size();
    }
    
    public double sigma(List<Double> mas) {
        double xs = xAverage(mas);
        double sko = 0;
        for (int i = 0; i < mas.size(); i++) {
            sko += Math.pow(mas.get(i) - xs , 2);
        }
        return Math.sqrt(sko / mas.size());
    }

    public double dispersion(List<Double> mas){
        return  Math.pow(sigma(mas), 2);
    }
    
    public double mediana(List<Double> mas){
        Collections.sort(mas);
        if(mas.size()%2 != 0){
            return mas.get((int)Math.ceil((mas.size()/2)));
        }else{
            return (mas.get(mas.size()/2) + mas.get((mas.size()/2) + 1))/2;
        }
    }
    
    public double interqartil(List<Double> mas){
        double secondQuartel = mediana(mas);
        List<Double> leftPart = new ArrayList();
        List<Double> rightPart = new ArrayList();
        for(int i = 0; i < mas.size(); i++){
            if(mas.get(i) < secondQuartel)
                leftPart.add(mas.get(i));
            if(mas.get(i) > secondQuartel)
                rightPart.add(mas.get(i));
        }
        return mediana(rightPart) - mediana(leftPart);
    }
    
    public double silvermanBandtwith(List<Double> mas) {
        double A = Math.min(sigma(mas), interqartil(mas)/1.34);
        return 0.9 * A * Math.pow(mas.size(), -0.2);
    }
    
    public ArrayList<Double> getCoridorValues(List<Double> gen){
        ArrayList<Double> coridors = new ArrayList<>();
        Collections.sort(gen);
        double h = (gen.get(gen.size() - 1) - gen.get(0)) / N;
        System.out.println(h);
        for (double i = gen.get(0); i <= gen.get(gen.size() - 1);) {
            coridors.add(i);
            i = i + h;
        }
        System.out.println("Массив X:");
        System.out.println(coridors);
        return coridors;
    }
    
    public ArrayList<ArrayList<Double>> getColsHeight(List<Double> gen, ArrayList<Double> coridors){
        ArrayList<ArrayList<Double>> cols = new ArrayList<>();
        for (int j = 0; j < coridors.size() - 1; j++) {
            ArrayList<Double> col = new ArrayList<>();
            for (int i = 0; i < gen.size(); i++) {
                if (gen.get(i) > coridors.get(j) && gen.get(i) <= coridors.get(j + 1)) {
                    col.add(gen.get(i));
                }
                if (i == gen.size() - 1) {
                    cols.add(col);
                }
            }
        }
        return cols;
    }
    
    public void checkPearson(){
        
    }
    
}

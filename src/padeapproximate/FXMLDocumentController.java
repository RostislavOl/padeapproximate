/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package padeapproximate;

import approximate.Approximator;
import approximate.MathOperations;
import constants.ListsConstants;
import data.SideEffects;
import data.generator;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.scene.chart.LineChart;
import javafx.scene.chart.XYChart;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TextField;
import javafx.scene.layout.Pane;

public class FXMLDocumentController {

    private SideEffects se = new SideEffects();

    private Approximator approximator = new Approximator();

    private MathOperations math = new MathOperations();

    File selectFile;

    @FXML
    private Button chooseFile;

    @FXML
    private ComboBox observableList;

    @FXML
    private Pane normPane;

    @FXML
    private TextField getA;

    @FXML
    private TextField getB;

    @FXML
    private Button genNormChart;

    @FXML
    private Pane exponentialPane;

    @FXML
    private TextField getAExp;

    @FXML
    private TextField getBExp;

    @FXML
    private TextField getLambdaExp;

    @FXML
    private Button genExpChart;

    @FXML
    private Pane couchiPane;

    @FXML
    private TextField getACouchi;

    @FXML
    private TextField getBCouchi;

    @FXML
    private Button genCouchiChart;

    @FXML
    private Pane leviPane;

    @FXML
    private TextField getMuLevi;

    @FXML
    private TextField getCLevi;

    @FXML
    private Button genLevi;

    @FXML
    private Pane genNormPane;

    @FXML
    private Button genNormBar;

    @FXML
    private Pane exponentialButtonPane;

    @FXML
    private Button exponentialCoridors;

    @FXML
    private Pane couchiButtonPane;

    @FXML
    private Button couchiCoridors;

    @FXML
    private Pane leviButtonPane;

    @FXML
    private Button leviCoridors;

    @FXML
    private LineChart coridorsChart;

    @FXML
    private TableColumn<XYChart.Data, Float> xColumn;

    @FXML
    private TableColumn<XYChart.Data, Integer> yColumn;

    @FXML
    private ComboBox kernelList;

    @FXML
    private TextField setN;

    @FXML
    private TextField setH;

    @FXML
    private TextField setM;

    @FXML
    private Pane logisticPane;

    @FXML
    private Button genLogChart;

    @FXML
    private TextField getMulog;

    @FXML
    private TextField getBlog;

    @FXML
    private Pane logButtonPane;

    @FXML
    private Button logisticCoridors;

    @FXML
    private Pane laplasianPane;

    @FXML
    private TextField getALap;

    @FXML
    private TextField getBLap;

    @FXML
    private TextField getLambdaLap;

    @FXML
    private TextField getMuLap;

    @FXML
    private Button genLapChart;

    @FXML
    private Pane lapButtonPane;

    @FXML
    private Button laplassianCoridors;
    
    @FXML
    private Pane skoPane;
    
    @FXML
    private TextField getSKO;
    
    ArrayList<Integer> y = new ArrayList<>();

    private ObservableList<XYChart.Data> pointsData = FXCollections.observableArrayList();

    private ObservableList<XYChart.Data> approximateData = FXCollections.observableArrayList();

    private generator gen = new generator();

    private double windowWidth = 0;

    private final ListsConstants constants = new ListsConstants();

    ArrayList<Double> coridors;

    List<Double> newy = new ArrayList<>();

    public void setPanelVisibility(boolean norm, boolean exp, boolean couchi, boolean levi, boolean log, boolean laplase) {
        normPane.setVisible(norm);
        exponentialPane.setVisible(exp);
        couchiPane.setVisible(couchi);
        leviPane.setVisible(levi);
        logisticPane.setVisible(log);
        laplasianPane.setVisible(laplase);
        if (!setN.getText().equals("")) {
            gen.setNum(new Integer(setN.getText()));
        }
        if (!setM.getText().equals("")) {
            math.setN(new Integer(setM.getText()));
        }
        if (!setH.getText().equals("")) {
            windowWidth = new Double(setH.getText());
        }
    }

    public void setVisibilityGenPane(boolean norm, boolean exp, boolean couchi, boolean levi, boolean log, boolean laplase) {
        genNormPane.setVisible(norm);
        exponentialButtonPane.setVisible(exp);
        couchiButtonPane.setVisible(couchi);
        leviButtonPane.setVisible(levi);
        logButtonPane.setVisible(log);
        lapButtonPane.setVisible(laplase);
    }

    @FXML
    public void initialize() {
        setN.setText("10000");
        setM.setText("15");
        chooseFile.setOnAction(e -> setFile());
        ObservableList<String> options
                = FXCollections.observableArrayList(
                        constants.RAVNOM_RASP,
                        constants.EXP_RASP,
                        constants.COCHI_RASP,
                        constants.LEVI_RASP,
                        constants.LOG_RASP,
                        constants.LAPLAS_RASP
                );
        observableList.setValue("Выберите закон распределения");
        observableList.setItems(options);

        ObservableList<String> kernels
                = FXCollections.observableArrayList(
                        constants.GAUSS_KERNEL,
                        constants.EXP_KERNEL,
                        constants.EPANECHNIKOV_KERNEL,
                        constants.COSINUS_KERNEL,
                        constants.TRIANGLE_KERNEL,
                        constants.THIRD_CUBE_KERNEL,
                        constants.SILVERMAN_KERNEL,
                        constants.SIGMOIDAL_KERNEL,
                        constants.THIRD_QUAD_KERNEL,
                        constants.FISHER_KERNEL
                );
        kernelList.setValue("Выберите ядерную функцию");
        kernelList.setItems(kernels);
        kernelList.setVisible(false);

        observableList.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent t) {
                if (observableList.getSelectionModel().getSelectedIndex() == 0) {
                    setPanelVisibility(true, false, false, false, false, false);
                }
                if (observableList.getSelectionModel().getSelectedIndex() == 1) {
                    setPanelVisibility(false, true, false, false, false, false);
                }
                if (observableList.getSelectionModel().getSelectedIndex() == 2) {
                    setPanelVisibility(false, false, true, false, false, false);
                }
                if (observableList.getSelectionModel().getSelectedIndex() == 3) {
                    setPanelVisibility(false, false, false, true, false, false);
                }
                if (observableList.getSelectionModel().getSelectedIndex() == 4) {
                    setPanelVisibility(false, false, false, false, true, false);
                }
                if (observableList.getSelectionModel().getSelectedIndex() == 5) {
                    setPanelVisibility(false, false, false, false, false, true);
                }
            }
        });

        genNormChart.setOnAction(e -> {
            gen.getRavnomernRand(new Double(getA.getText()), new Double(getB.getText()));
            System.out.println(gen.getGeneratedMas());
            setVisibilityGenPane(true, false, false, false, false, false);
        });
        genExpChart.setOnAction(e -> {
            gen.getExpRand(new Double(getAExp.getText()), new Double(getBExp.getText()), new Double(getLambdaExp.getText()));
            System.out.println(gen.getGeneratedMas());
            setVisibilityGenPane(false, true, false, false, false, false);
        });
        genCouchiChart.setOnAction(e -> {
            gen.getCoshiRand(new Double(getACouchi.getText()), new Double(getBCouchi.getText()));
            System.out.println(gen.getGeneratedMas());
            setVisibilityGenPane(false, false, true, false, false, false);
        });
        genLevi.setOnAction(e -> {
            gen.getLeviRand(new Double(getMuLevi.getText()), new Double(getCLevi.getText()));
            System.out.println(gen.getGeneratedMas());
            setVisibilityGenPane(false, false, false, true, false, false);
        });
        genLogChart.setOnAction(e -> {
            gen.getLogisticRand(new Double(getMulog.getText()), new Double(getBlog.getText()));
            System.out.println(gen.getGeneratedMas());
            setVisibilityGenPane(false, false, false, false, true, false);
        });
        genLapChart.setOnAction(e -> {
            gen.getLaplassianRand(new Double(getALap.getText()), new Double(getBLap.getText()), new Double(getLambdaLap.getText()), new Double(getMuLap.getText()));
            System.out.println(gen.getGeneratedMas());
            setVisibilityGenPane(false, false, false, false, false, true);
        });

        kernelList.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent t) {
                if (kernelList.getSelectionModel().getSelectedIndex() == 0) {
                    newy = approximator.gaussianKernel(coridors, windowWidth);
                    System.out.println("СКО новое:");
                    System.out.println(math.sigma(newy));
                    approximateChart();
                    skoPane.setVisible(true);
                }
                if (kernelList.getSelectionModel().getSelectedIndex() == 1) {
                    newy = approximator.exponentialKernel(coridors, windowWidth);
                    System.out.println("СКО новое:");
                    System.out.println(math.sigma(newy));
                    approximateChart();
                    skoPane.setVisible(true);
                }
                if (kernelList.getSelectionModel().getSelectedIndex() == 2) {
                    newy = approximator.epanechnikovKernel(coridors, windowWidth);
                    System.out.println("СКО новое:");
                    System.out.println(math.sigma(newy));
                    approximateChart();
                    skoPane.setVisible(true);
                }
                if (kernelList.getSelectionModel().getSelectedIndex() == 3) {
                    newy = approximator.cosinusKernel(coridors, windowWidth);
                    System.out.println("СКО новое:");
                    System.out.println(math.sigma(newy));
                    approximateChart();
                    skoPane.setVisible(true);
                }
                if (kernelList.getSelectionModel().getSelectedIndex() == 4) {
                    newy = approximator.triangeleKernel(coridors, windowWidth);
                    System.out.println("СКО новое:");
                    System.out.println(math.sigma(newy));
                    approximateChart();
                    skoPane.setVisible(true);
                }
                if (kernelList.getSelectionModel().getSelectedIndex() == 5) {
                    newy = approximator.threeCubeKernel(coridors, windowWidth);
                    System.out.println("СКО новое:");
                    System.out.println(math.sigma(newy));
                    approximateChart();
                    skoPane.setVisible(true);
                }
                if (kernelList.getSelectionModel().getSelectedIndex() == 6) {
                    newy = approximator.silvermanKernel(coridors, windowWidth);
                    System.out.println("СКО новое:");
                    System.out.println(math.sigma(newy));
                    approximateChart();
                    skoPane.setVisible(true);
                }
                if (kernelList.getSelectionModel().getSelectedIndex() == 7) {
                    newy = approximator.sigmoidalKernel(coridors, windowWidth);
                    System.out.println("СКО новое:");
                    System.out.println(math.sigma(newy));
                    approximateChart();
                    skoPane.setVisible(true);
                }
                if (kernelList.getSelectionModel().getSelectedIndex() == 8) {
                    newy = approximator.threeQuadKernell(coridors, windowWidth);
                    System.out.println("СКО новое:");
                    System.out.println(math.sigma(newy));
                    approximateChart();
                    skoPane.setVisible(true);
                }
                if (kernelList.getSelectionModel().getSelectedIndex() == 9) {
                    newy = approximator.fisherKernell(coridors, windowWidth);
                    System.out.println("СКО новое:");
                    System.out.println(math.sigma(newy));
                    approximateChart();
                    skoPane.setVisible(true);
                }
                
                System.out.println("Плотность распределения вероятности:");
                System.out.println(newy);
            }
        });

        genNormBar.setOnAction(e -> calcHist());
        exponentialCoridors.setOnAction(e -> calcHist());
        couchiCoridors.setOnAction(e -> calcHist());
        leviCoridors.setOnAction(e -> calcHist());
        logisticCoridors.setOnAction(e -> calcHist());
        laplassianCoridors.setOnAction(e -> calcHist());
        //xColumn.setCellValueFactory(cellData -> cellData.getValue().XValueProperty());
        //yColumn.setCellValueFactory(cellData -> cellData.getValue().YValueProperty());
        XYChart.Series series = new XYChart.Series();
        XYChart.Series approximateSeries = new XYChart.Series();
        series.setData(pointsData);
        approximateSeries.setData(approximateData);
        coridorsChart.getData().add(series);
        coridorsChart.getData().add(approximateSeries);
    }

    private void setFile() {
        selectFile = se.getFile();
        try {
            gen.getFloatFromFile(selectFile);
            System.out.println(gen.getGeneratedMas());
            calcHist();
        } catch (FileNotFoundException ex) {
            System.out.println("File is not choosen");
        }

    }

    private void calcHist() {
        kernelList.setVisible(true);
        coridors = math.getCoridorValues(gen.getGeneratedMas());
        ArrayList<ArrayList<Double>> cols = math.getColsHeight(gen.getGeneratedMas(), coridors);
        for (int i = 0; i < cols.size(); i++) {
            y.add(cols.get(i).size());
        }
        System.out.println("Массив Y:");
        System.out.println(y);

        for (int i = 0; i < coridors.size() - 1; i++) {
            XYChart.Data data = new XYChart.Data(coridors.get(i), y.get(i));
            pointsData.add(data);
        }

        System.out.println("СКО старое:");
        System.out.println(math.sigma(coridors));
        getSKO.setText(math.sigma(coridors) + "");

    }

    private void approximateChart() {
        for (int i = 0; i < newy.size() - 1; i++) {
            XYChart.Data approxChart = new XYChart.Data(newy.get(i), y.get(i));
            approximateData.add(approxChart);
        }
    }
}
